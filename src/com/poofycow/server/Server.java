package com.poofycow.server;


public interface Server {
    public void start();
    public void kill();
    public boolean isRunning();
}
